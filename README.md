This is a basic cron-like utility that allows the user to schedule operations by
using some natural language syntax.

The simple scheduler will take a string which describes when the operation should
execute, an optional description and finally the operation itself.

